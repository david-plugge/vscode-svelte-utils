import { FileType, ResourceType } from './enum';
import { TemplateConfig } from './types/templates';
import { mkdir, writeFile } from 'fs/promises';
import { join } from 'path';

import page from './templates/page';
import pageLoad from './templates/page-load';
import pageServer from './templates/page-server';
import layout from './templates/layout';
import layoutLoad from './templates/layout-load';
import layoutServer from './templates/layout-server';
import error from './templates/error';
import server from './templates/server';

const typeMap = new Map([
    [ResourceType.PAGE, { generate: page, filename: '+page', type: FileType.PAGE }],
    [ResourceType.PAGE_LOAD, { generate: pageLoad, filename: '+page', type: FileType.SCRIPT }],
    [ResourceType.PAGE_SERVER, { generate: pageServer, filename: '+page.server', type: FileType.SCRIPT }],
    [ResourceType.LAYOUT, { generate: layout, filename: '+layout', type: FileType.PAGE }],
    [ResourceType.LAYOUT_LOAD, { generate: layoutLoad, filename: '+layout', type: FileType.SCRIPT }],
    [ResourceType.LAYOUT_SERVER, { generate: layoutServer, filename: '+layout.server', type: FileType.SCRIPT }],
    [ResourceType.SERVER, { generate: server, filename: '+server', type: FileType.SCRIPT }],
    [ResourceType.ERROR, { generate: error, filename: '+error', type: FileType.PAGE }],
]);

export async function generateResources(config: TemplateConfig) {
    await mkdir(config.path, { recursive: true });

    const promises: Promise<string>[] = [];

    for (const type of config.resources) {
        const template = typeMap.get(type);
        if (!template) {
            throw new Error(`no template found for '${type}'`);
        }
        const ext = template.type === FileType.PAGE ? config.pageExtension : config.scriptExtension;
        const filepath = join(config.path, `${template.filename}.${ext}`);

        promises.push(
            template
                .generate(config)
                .then((data) => writeFile(filepath, data))
                .then(() => filepath),
        );
    }

    const writtenFiles = await Promise.all(promises);
    return writtenFiles;
}
