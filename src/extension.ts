import { commands, ExtensionContext, Uri, window, workspace } from 'vscode';
import { commandsMap } from './commands';
import { ResourceType } from './enum';
import * as path from 'path';
import { generateResources } from './sveltekit';
import { findFile } from './utils';
import { TemplateConfig } from './types/templates';
import { ICommand } from './types/command';

export function activate(context: ExtensionContext) {
    const showDynamicDialog = async (uri: Uri | undefined, command: ICommand) => {
        let resources: ResourceType[] = command.resources;
        if (!resources?.length) {
            const options = [ResourceType.PAGE, ResourceType.PAGE_LOAD, ResourceType.PAGE_SERVER, ResourceType.LAYOUT, ResourceType.LAYOUT_LOAD, ResourceType.LAYOUT_SERVER, ResourceType.ERROR];
            const result = await window.showQuickPick(options, { canPickMany: true, title: 'Select files to generate' });
            if (!result) {
                return;
            }
            resources = result as ResourceType[];
        }

        const itemPath = await window.showInputBox({
            prompt: `Type the path of the new ${resources.length === 1 ? resources[0] : 'files'}`,
            value: '/',
        });

        if (!itemPath) {
            throw new Error('Please enter a path');
        }

        let rootPath!: string;

        if (uri) {
            rootPath = uri.fsPath;
        } else if (window.activeTextEditor) {
            rootPath = path.dirname(window.activeTextEditor.document.fileName);
        } else if (workspace.workspaceFolders && workspace.workspaceFolders.length === 1) {
            rootPath = workspace.workspaceFolders[0].uri.fsPath;
        }

        if (!rootPath) {
            await window.showErrorMessage('Could not resolve root path. Please open a file first or use the context menu!');
            return;
        }

        const isTypescript = !!findFile(rootPath, 'tsconfig.json');
        const fullPath = path.join(rootPath, itemPath);

        const config: TemplateConfig = {
            path: fullPath,
            typescript: isTypescript,
            resources: resources,
            pageExtension: 'svelte',
            scriptExtension: isTypescript ? 'ts' : 'js',
        };

        const writtenFiles = await generateResources(config);
        if (writtenFiles[0]) {
            const openUri = Uri.parse(writtenFiles[0]);
            workspace.openTextDocument(openUri).then((doc) => window.showTextDocument(doc));
        }
    };

    for (const [key, value] of commandsMap) {
        const command = commands.registerCommand(key, (args) => showDynamicDialog(args, value));
        context.subscriptions.push(command);
    }
}

// this method is called when your extension is deactivated
export function deactivate() {}
