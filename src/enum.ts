export enum CommandType {
    PAGE = 'extension.addSvelteKitPage',
    PAGE_LOAD = 'extension.addSvelteKitPageLoad',
    PAGE_SERVER = 'extension.addSvelteKitPageServerLoad',
    LAYOUT = 'extension.addSvelteKitLayout',
    LAYOUT_LOAD = 'extension.addSvelteKitLayoutLoad',
    LAYOUT_SERVER = 'extension.addSvelteKitLayoutServerLoad',
    SERVER = 'extension.addSvelteKitServer',
    ERROR = 'extension.addSvelteKitError',

    MULTIPLE = 'extension.addSvelteKitMultiple',
}

export enum FileType {
    SCRIPT,
    PAGE,
}

export enum ResourceType {
    PAGE = 'Page',
    PAGE_LOAD = 'Page load',
    PAGE_SERVER = 'Page server load',
    LAYOUT = 'Layout',
    LAYOUT_LOAD = 'Layout load',
    LAYOUT_SERVER = 'Layout server load',
    SERVER = 'Server',
    ERROR = 'Error',
}
