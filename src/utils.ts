import { existsSync } from 'fs';
import * as path from 'path';

export function findFile(searchPath: string, fileName: string) {
    while (true) {
        const filePath = path.join(searchPath, fileName);
        if (existsSync(filePath)) {
            return filePath;
        }
        const parentPath = path.dirname(searchPath);
        if (parentPath === searchPath) {
            return;
        }
        searchPath = parentPath;
    }
}
