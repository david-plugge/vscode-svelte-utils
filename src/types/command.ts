import { ResourceType } from '../enum';

export interface ICommand {
    resources: ResourceType[];
}
