import { ResourceType } from '../enum';

export interface TemplateConfig {
    path: string;
    typescript: boolean;
    resources: ResourceType[];
    pageExtension: string;
    scriptExtension: string;
}
